<?php

/**
 * Field handler to provide simple renderer that allows linking to a node.
 */
class views_handler_field_ft_legacy extends views_handler_field_node {
  function render($values) {
    $custom_title = ft_legacy_get($values->nid) .'';
    if (strlen($custom_title)) {
      $values->{$this->field_alias} = $custom_title;
    }
    return $this->render_link($values->{$this->field_alias}, $values);
  }
}
